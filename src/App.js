import { useState } from 'react';
import './App.css';
import { Alert, Backdrop, Box, Button, CircularProgress, FormHelperText, TextField, Typography } from '@mui/material';
import axios from 'axios';
import CheckIcon from '@mui/icons-material/Check';

function App() {
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState({
    Nome: null,
    Email: null,
    Telefone: null,
    Descricao: null
  });
  const [responseData, setResponseData] = useState(null);
  const [error, setError] = useState(null);
  const [sucesso, setsucesso] = useState(null);
  // const handleClose = () => {
  //   setOpen(false);
  // };

  const apiUrl = 'https://emailpushteste.onrender.com/api/Email';
  // const apiUrl = 'http://localhost:32771/api/Email';
  const handleOpen = async (event) => {
    event.preventDefault();
    setOpen(true);
    console.log(form)
    try {
      const response = await axios.post(apiUrl, form);
      setResponseData(response.data);
      setError(null);
      setsucesso("Email enviado com sucesso")
      setOpen(false);
      setForm({
        Nome: '',
        Email: '',
        Telefone: '',
        Descricao: ''
      });
    } catch (error) {
      setError("Houve um problema ao enviar seu email, tente novamente ou entre em contato com o suporte em caso de percistencia");
      setOpen(false);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <Box
      display="flex"
      alignItems="center"
      flexDirection="column"
    >

      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
      // onClick={handleClose}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Box
        minHeight={'50vh'}
        minWidth={"50%"}
        my={3}
        display="flex"
        alignItems="center"
        flexDirection="column"
        gap={4}
        p={2}
        sx={{
          boxShadow: '5px 5px 15px 5px rgba(0, 0, 0, 0.3)',
          borderRadius: '15px',
          display: 'flex'
        }}
      >
        <Typography variant="h4">
          Formulário De Cadastro De Clientes
        </Typography>
        <Typography variant="h6">
          Este formulario é usado apenas para o cadastro, e divisão de clientes por setor
        </Typography>
        {sucesso != null &&
          <Alert severity="success">
            {sucesso}
          </Alert>
        }
        {error != null &&
          <Alert severity="error">
            {error}
          </Alert>
        }
        <TextField
          name="Nome"
          onChange={handleChange}
          label="Nome"
          color="secondary"
          focused
          value={form.Nome}
          sx={{
            width: '80%'
          }}
        />
        <FormHelperText />
        <TextField
          name="Telefone"
          onChange={handleChange}
          label="Telefone"
          color="secondary"
          value={form.Telefone}
          focused
          sx={{
            width: '80%'
          }}
        />
        <FormHelperText />
        <TextField
          name="Email"
          onChange={handleChange}
          value={form.Email}
          label="E-mail"
          color="secondary"
          focused
          sx={{
            width: '80%'
          }}
        />
        <FormHelperText />
        <TextField
          name="Descricao"
          onChange={handleChange}
          label="O que deseja"
          color="secondary"
          value={form.Descricao}
          multiline
          rows={5}
          focused
          sx={{
            width: '80%'
          }}
          placeholder='Informe sua duvida, solicite o contato de um atendente'
        />
        <FormHelperText />
        <Button variant="contained" onClick={handleOpen}>Enviar</Button>
      </Box>
    </Box>
  );
}

export default App;
